package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//import controller.ControlaFrame;

public class MenuInicialFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PotenciaHarmonicaFrame frame = new PotenciaHarmonicaFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuInicialFrame() {
		setTitle("Aprenda QEE");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setName("");
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		
		JButton simulacao1 = new JButton("Simular fluxo de potência fundamental");
		simulacao1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				//Add action
			}
		});
		simulacao1.setForeground(Color.WHITE);
		simulacao1.setBackground(new Color(102, 102, 255));
		simulacao1.setMinimumSize(new Dimension(180, 40));
		simulacao1.setMaximumSize(new Dimension(180, 40));
		simulacao1.setFont(new Font("Ubuntu", Font.BOLD, 14));
		simulacao1.setPreferredSize(new Dimension(200, 40));
		
		JButton simulacao2 = new JButton("Simular distorção harmônica");
		simulacao2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				//Add action
			}
		});
		simulacao2.setBackground(new Color(102, 102, 255));
		simulacao2.setForeground(Color.WHITE);
		simulacao2.setMinimumSize(new Dimension(180, 40));
		simulacao2.setMaximumSize(new Dimension(180, 40));
		simulacao2.setFont(new Font("Ubuntu", Font.BOLD, 14));
		simulacao2.setPreferredSize(new Dimension(200, 40));
		
		JButton simulacao3 = new JButton("Simular fluxo de potência harmônico");
		simulacao3.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				//Add action
			}
		});
		simulacao3.setMinimumSize(new Dimension(180, 40));
		simulacao3.setMaximumSize(new Dimension(180, 40));
		simulacao3.setFont(new Font("Ubuntu", Font.BOLD, 14));
		simulacao3.setPreferredSize(new Dimension(200, 40));
		simulacao3.setForeground(Color.WHITE);
		simulacao3.setBackground(new Color(102, 102, 255));
		
		JLabel descricao = new JLabel("Selecione uma simulação");
		descricao.setFont(new Font("Ubuntu", Font.BOLD, 16));
		descricao.setHorizontalTextPosition(SwingConstants.CENTER);
		descricao.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel tituloPanel = new JPanel();
		tituloPanel.setEnabled(false);
		tituloPanel.setBackground(UIManager.getColor("List.dropLineColor"));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addGap(0, 12, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(descricao, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(tituloPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE))
					.addGap(455))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(238)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(simulacao3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(simulacao2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(simulacao1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE))
					.addContainerGap(213, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(tituloPanel, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addGap(42)
					.addComponent(descricao)
					.addGap(62)
					.addComponent(simulacao1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(simulacao2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(simulacao3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(158, Short.MAX_VALUE))
		);
		
		JLabel titulo = new JLabel("Aprenda QEE");
		titulo.setForeground(Color.WHITE);
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBackground(Color.WHITE);
		titulo.setFont(new Font("Ubuntu", Font.BOLD, 28));
		titulo.setHorizontalTextPosition(SwingConstants.CENTER);
		GroupLayout gl_tituloPanel = new GroupLayout(tituloPanel);
		gl_tituloPanel.setHorizontalGroup(
			gl_tituloPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_tituloPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(titulo)
					.addContainerGap(1014, Short.MAX_VALUE))
		);
		gl_tituloPanel.setVerticalGroup(
			gl_tituloPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_tituloPanel.createSequentialGroup()
					.addContainerGap(59, Short.MAX_VALUE)
					.addComponent(titulo)
					.addContainerGap())
		);
		tituloPanel.setLayout(gl_tituloPanel);
		contentPane.setLayout(gl_contentPane);
	}
}
