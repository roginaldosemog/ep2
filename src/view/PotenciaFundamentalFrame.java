package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PotenciaFundamentalFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PotenciaFundamentalFrame frame = new PotenciaFundamentalFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PotenciaFundamentalFrame() {
		setTitle("Aprenda QEE");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setName("");
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		
		JLabel descricao = new JLabel("Potência Fundamental");
		descricao.setFont(new Font("Ubuntu", Font.BOLD, 16));
		descricao.setHorizontalTextPosition(SwingConstants.CENTER);
		descricao.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel tituloPanel = new JPanel();
		tituloPanel.setEnabled(false);
		tituloPanel.setBackground(new Color(100, 149, 237));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(0, 0, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(descricao, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(tituloPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE))
					.addGap(455))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(tituloPanel, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addGap(42)
					.addComponent(descricao)
					.addContainerGap(376, Short.MAX_VALUE))
		);
		
		JLabel titulo = new JLabel("Aprenda QEE");
		titulo.setForeground(Color.WHITE);
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBackground(Color.WHITE);
		titulo.setFont(new Font("Ubuntu", Font.BOLD, 28));
		titulo.setHorizontalTextPosition(SwingConstants.CENTER);
		
		JButton btnVoltar = new JButton("VOLTAR AO MENU");
		btnVoltar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
			}
		});
		btnVoltar.setFont(new Font("Ubuntu", Font.BOLD, 12));
		btnVoltar.setForeground(UIManager.getColor("List.background"));
		btnVoltar.setBackground(new Color(100, 149, 237));
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GroupLayout gl_tituloPanel = new GroupLayout(tituloPanel);
		gl_tituloPanel.setHorizontalGroup(
			gl_tituloPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_tituloPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(titulo)
					.addPreferredGap(ComponentPlacement.RELATED, 490, Short.MAX_VALUE)
					.addComponent(btnVoltar)
					.addGap(36))
		);
		gl_tituloPanel.setVerticalGroup(
			gl_tituloPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_tituloPanel.createSequentialGroup()
					.addContainerGap(15, Short.MAX_VALUE)
					.addGroup(gl_tituloPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_tituloPanel.createSequentialGroup()
							.addComponent(btnVoltar, GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, gl_tituloPanel.createSequentialGroup()
							.addComponent(titulo)
							.addGap(14))))
		);
		tituloPanel.setLayout(gl_tituloPanel);
		contentPane.setLayout(gl_contentPane);
	}
}
