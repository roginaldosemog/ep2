# Aprender QEE
O Aprenda QEE fornece de forma gratuita a simulação de vários
fenômenos da disciplina de QEE, facilitando o entendimento do conteúdo pelos
alunos da Engenharia de Energia e, também, facilitando a exposição dos conceitos
pelo discente durante as aulas da disciplina de QEE.

## Funcionalidades do programa
O sistema oferece as seguintes funcionalidades ao usuário:
- Simular fluxo de potência fundamental
- Simular distorção harmônica
- Simular fluxo de potência harmônico

## Como executar a aplicação
- Faça o download do projeto, ou execute o comando abaixo dentro da pasta desejada.
```
git clone https://gitlab.com/roginaldosemog/ep2
```
- Inicie o programa Eclipse (recomenda-se a versão Oxygen 4.7.1a), e importe o projeto.
- Após importar o projeto, execute-o clicando no botão 'Run ep2'.
- Após clicar, a janela da aplicação abrirá.
